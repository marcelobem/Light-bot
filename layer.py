from yowsup.layers.interface                           import YowInterfaceLayer, ProtocolEntityCallback
from yowsup.layers.protocol_messages.protocolentities  import TextMessageProtocolEntity
from yowsup.layers.protocol_receipts.protocolentities  import OutgoingReceiptProtocolEntity
from yowsup.layers.protocol_acks.protocolentities      import OutgoingAckProtocolEntity
from rivescript import RiveScript
from yowsup.layers.protocol_media.protocolentities  import *
import re
import json
import math
import urllib2
import urllib
bot = RiveScript()
bot.load_directory("./brain")
bot.sort_replies()

class EchoLayer(YowInterfaceLayer):

    @ProtocolEntityCallback("message")
    def onMessage(self, messageProtocolEntity):

        if messageProtocolEntity.getType() == 'text':
            self.onTextMessage(messageProtocolEntity)
        elif messageProtocolEntity.getType() == 'media':
            self.onMediaMessage(messageProtocolEntity)
        else:
            self.onLocationMessage(messageProtocolEntity)

        


    @ProtocolEntityCallback("receipt")
    def onReceipt(self, entity):
        self.toLower(entity.ack())

    def onTextMessage(self,messageProtocolEntity):
        # just print info
        message = messageProtocolEntity.getBody();
        sender = messageProtocolEntity.getFrom();
        message = unicode(message,"utf8")

        print("Recieved %s from %s" % (message, sender))
        if(message == '/reload'):
            self.reloadBrain()
            reply = "SYS : Brain Reloaded."
        else:
            reply = bot.reply(sender, message)
        outgoingMessageProtocolEntity = TextMessageProtocolEntity(
                reply,
                to = sender)
        self.toLower(outgoingMessageProtocolEntity)
        self.toLower(messageProtocolEntity.ack())
        self.toLower(messageProtocolEntity.ack(True))

    def onMediaMessage(self, messageProtocolEntity):
        # just print info
        if messageProtocolEntity.getMediaType() == "image":
            print("Echoing image %s to %s" % (messageProtocolEntity.url, messageProtocolEntity.getFrom(False)))

        elif messageProtocolEntity.getMediaType() == "location":
            s_lat = messageProtocolEntity.getLatitude()
            s_lng = messageProtocolEntity.getLongitude()
            sender = messageProtocolEntity.getFrom();
            print("Echoing location (%s, %s) to %s" % (s_lat, s_lng, messageProtocolEntity.getFrom(False)))
            message = 'mylocation ' + self.breakDecimal(s_lat) + " " + self.breakDecimal(s_lng)
            print(message)

            reply = bot.reply(sender, message)
            
            if reply[0:4] == 'http':
                
                print(reply)
                response = urllib2.urlopen(reply)
    
                raw_data = json.loads(response.read())
                for i in range(len(raw_data['response']['venues'])):
                    latitude = raw_data['response']['venues'][i]['location']['lat']
                    
                    longitude= raw_data['response']['venues'][i]['location']['lng']
                    locationName = raw_data['response']['venues'][i]['name'] 
                    outLocation = LocationMediaMessageProtocolEntity(latitude,
                    longitude,locationName,
                    None, 'raw',
                    to = messageProtocolEntity.getFrom())
                    self.toLower(outLocation)

                
        elif messageProtocolEntity.getMediaType() == "vcard":
            print("Echoing vcard (%s, %s) to %s" % (messageProtocolEntity.getName(), messageProtocolEntity.getCardData(), messageProtocolEntity.getFrom(False)))
        self.toLower(messageProtocolEntity.ack())
        self.toLower(messageProtocolEntity.ack(True))


    def onLocationMessage(self, messageProtocolEntity):
        print("Type %s" % (messageProtocolEntity.getType()) )
        
    def breakDecimal(self,value):
        value = str(value)
        i = value.index('.');
        return value[:i] + " " + value[i+1:]     

    def reloadBrain(self):
        bot.load_directory("./brain")
        print("brain reloaded")
    